var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;

let models = require('../Models')
var User = models.user
var Clients = models.client
var AccessToken = models.AccessToken
var RefreshToken = models.RefreshToken

// 2 Client Password strategies - 1st is required, 2nd is optional
// https://tools.ietf.org/html/draft-ietf-oauth-v2-27#section-2.3.1

// Client Password - HTTP Basic authentication
passport.use(new BasicStrategy(
    function (username, password, done) {

        Clients.findOne({
            where: {
                clientId: username
            }
        }).then(function (client) {

            if (!client) {
                return done(null, false);
            }

            if (client.clientSecret !== password) {
                return done(null, false);
            }

            return done(null, client.get());

        }).catch(err => {
            console.log(err.stack);
            return done(err);
        });

    }
));

// Client Password - credentials in the request body
passport.use(new ClientPasswordStrategy(
    function (clientId, clientSecret, done) {

        Clients.findOne({
            where: {
                clientId: clientId
            }
        }).then(function (client) {
            if (!client) {
                return done(null, false);
            }

            if (client.clientSecret !== clientSecret) {
                return done(null, false);
            }

            return done(null, client);

        }).catch(err => {
            console.log(err.stack);
            return done(err);
        });
    }
));

// Bearer Token strategy
// https://tools.ietf.org/html/rfc6750

passport.use(new BearerStrategy(
    function (accessToken, done) {

        AccessToken.findOne({
            where: {
                token: accessToken
            }
        }).then(token => {
            if (!token) {
                return done(null, false);
            }
            User.findById(token.userId).then(user => {
                if (!user) {
                    return done(null, false, {message: 'Unknown user'});
                }

                var info = {scope: '*', mobile: token.mobile};
                done(null, user, info);
            })

        }).catch(err => {
            console.log(err.stack);
            return done(err);
        })

    }
));