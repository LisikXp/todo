var oauth2orize = require('oauth2orize');
var passport = require('passport');
var crypto = require('crypto');
let bCrypt = require('bcrypt-nodejs');

let models = require('../Models')
var User = models.user
var Clients = models.client
var AccessToken = models.AccessToken
var RefreshToken = models.RefreshToken

// Create OAuth 2.0 server
var aserver = oauth2orize.createServer();

// Generic error handler
var errFn = function (cb, err) {
    if (err) {
        return cb(err);
    }
};

// Destroy any old tokens and generates a new access and refresh token
var generateTokens = function (data, done) {

    AccessToken.destroy({
        where: {
            userId: data.userId,
            mobile: data.mobile
        }
    }).then(function (rowDeleted) { // rowDeleted will return number of rows deleted
        if (rowDeleted === 1) {
            console.log('Deleted successfully');
        }
    }, function (err) {
        console.log(err);
        return done(err);
    });
    RefreshToken.destroy({
        where: {
            userId: data.userId,
            mobile: data.mobile
        }
    }).then(function (rowDeleted) { // rowDeleted will return number of rows deleted
        if (rowDeleted === 1) {
            console.log('Deleted successfully');
        }
    }, function (err) {
        console.log(err);
        return done(err);
    });


    // Curries in `done` callback so we don't need to pass it
    var errorHandler = errFn.bind(undefined, done),
        refreshToken,
        refreshTokenValue,
        token,
        tokenValue;

    /*reftoken.remove(data, errorHandler);
    acctoken.remove(data, errorHandler);*/

    tokenValue = crypto.randomBytes(32).toString('hex');
    refreshTokenValue = crypto.randomBytes(32).toString('hex');

    data.token = tokenValue;

    AccessToken.create(data).then(task => {
        done(null, tokenValue, refreshTokenValue, {
            'expires_in': '10800'
        });
    }).catch(err => {
        return done(err);
    })

};

// Exchange username & password for access token
aserver.exchange(oauth2orize.exchange.password(function (client, username, password, scope, done) {

    var isValidPassword = function (userpass, password) {

        return bCrypt.compareSync(password, userpass);

    }


    User.findOne({
        where: {
            email: username
        }
    }).then(user => {

        if (user) {

            if (!user || !isValidPassword(user.password, password)) {
                return done(null, false);
            }

            var model = {
                userId: user.id,
                clientId: client.clientId,
                mobile: client.mobile
            };

             generateTokens(model, done);
        }

    }).catch(err => {
        return done(err);
    })

}));

// token endpoint
//
// `token` middleware handles client requests to exchange authorization grants
// for access tokens. Based on the grant type being exchanged, the above
// exchange middleware will be invoked to handle the request. Clients must
// authenticate when making requests to this endpoint.

module.exports.token = [
    passport.authenticate(['basic', 'oauth2-client-password'], {session: false}),
    aserver.token(),
    aserver.errorHandler()
];