module.exports = function(sequelize, Sequelize) {

    var Client = sequelize.define('client', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        clientId: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        clientSecret: {
            type: Sequelize.STRING
        },
        mobile:{
            type:Sequelize.BOOLEAN,
            default:false
        },

    });

    return Client;

}