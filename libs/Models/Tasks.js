module.exports = function(sequelize, Sequelize) {

    var Tasks = sequelize.define('task', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        userId: {
            type: Sequelize.INTEGER
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        project: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        time: {
            type: Sequelize.INTEGER
        },
        complete:{
            type:Sequelize.BOOLEAN,
            default:false
        },

    });

    return Tasks;

}