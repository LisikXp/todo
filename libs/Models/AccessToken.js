module.exports = function(sequelize, Sequelize) {

    var AccessToken = sequelize.define('AccessToken', {

        id: {
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        userId: {
            type: Sequelize.INTEGER,
            required: true
        },

        clientId: {
            type: Sequelize.STRING,
            required: true
        },

        token: {
            type: Sequelize.STRING,
            unique: true,
            required: true
        },

        mobile:{
            type:Sequelize.BOOLEAN,
            default:false
        }
    });

    return AccessToken;

}