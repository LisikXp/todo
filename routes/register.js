var express = require('express');
let bCrypt = require('bcrypt-nodejs');
let models = require('../libs/Models')
var router = express.Router();

/* GET home page. */
router.post('/', function (req, res, next) {
    let email = req.body.email
    let password = req.body.password
    let username = req.body.username
    let User = models.user

    User.findOne({
        where: {
            email: email
        }
    }).then(function (user) {

        if (user) {
            res.send({status: 'error', msg: 'That email is already taken'});
        } else {

            var userPassword = generateHash(password);

            var data = {
                email: email,
                password: userPassword,
                username: username,
            };

            User.create(data).then(function (newUser, created) {

                if (!newUser) {
                    console.log('error');
                    res.send({status: 'error'});
                }

                if (newUser) {
                    res.send({status: 'OK', users: newUser});
                }

            });

        }

    });
});

var generateHash = function (password) {

    return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);

};
module.exports = router;
