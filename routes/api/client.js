var express = require('express');
let models = require('../../libs/Models')

var router = express.Router();



router.post('/', function (req, res, next) {
    let Client = models.client


    var data = {
        name: req.body.name,
        clientId: req.body.clientId,
        clientSecret: req.body.clientSecret,
        mobile:req.body.mobile
    };

    Client.create(data).then(function (newUser, created) {

        if (!newUser) {
            console.log('error');
            res.send({status: 'error'});
        }

        if (newUser) {
            res.send({status: 'OK', client: newUser});
        }

    });

});


module.exports = router;