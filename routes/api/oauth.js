var express = require('express');


var oauth2 = require('../../libs/auth/oauth2');
var router = express.Router();

router.post('/', oauth2.token);

module.exports = router;