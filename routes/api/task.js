var express = require('express');
var passport = require('passport');
let models = require('../../libs/Models')
var router = express.Router();

router.get('/', passport.authenticate('bearer', {session: false}), function (req, res, next) {
    let Task = models.task
    // console.log(req.user.id);
    let uid = req.user.id
    Task.findAll({
        where: {
            userId: uid
        }
    }).then(function (tasks) {

        res.send({tasks: tasks});
    });

});
router.post('/', passport.authenticate('bearer', {session: false}), function (req, res, next) {
    let Task = models.task

    var data = {
        userId: req.user.id,
        name: req.body.name,
        project: req.body.project,
        time: req.body.time,
        complete: req.body.complete
    };

    Task.create(data).then(function (tasks, created) {

        if (!tasks) {
            console.log('error');
            res.send({status: 'error'});
        }

        if (tasks) {
            res.send({status: 'OK', task: tasks});
        }

    });

});

router.put('/:id', passport.authenticate('bearer', {session: false}), function (req, res, next) {
    let Task = models.task
    let id = req.params.id


    Task.update(
        {complete: req.body.complete},
        {where: {id: id}}
    ).then(result => {
        console.log(result);
        res.send({status: 'OK'});
    }).catch(err => {
        console.log(err)
        res.send({status: 'ERROR', msg: err});
    })
});
router.delete('/:id', passport.authenticate('bearer', {session: false}), function (req, res, next) {
    let Task = models.task
    let id = req.params.id

    Task.destroy({where: {id: id}})
        .then(function () {
            res.send({status: 'OK'});
        })
});

module.exports = router;