var express = require('express');
var createError = require('http-errors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var env = require('dotenv').load();
var passport = require('passport')
var session = require('express-session')
var bodyParser = require('body-parser')
var exphbs = require('express-handlebars')
var cors = require('cors');
var sassMiddleware = require('node-sass-middleware');
var models = require("./libs/Models");

require('./libs/auth/auth');
var oauth2 = require('./libs/auth/oauth2');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/api/users');
var registerRoute = require('./routes/register')
var clientAppRouter = require('./routes/api/client')
var oauthRoute = require('./routes/api/oauth')
var tasksRoute = require('./routes/api/task')

var app = express();
app.use(cors());
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(session({secret: 'keyboard cat', resave: true, saveUninitialized: true})); // session secret

app.use(passport.initialize());

app.use(passport.session());
app.use(sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true, // true = .sass and false = .scss
    sourceMap: true
}));
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/register', registerRoute);
app.use('/api/client', clientAppRouter);
app.use('/oauth/token', oauthRoute);
app.use('/api/task', tasksRoute);

//Sync Database
models.sequelize.sync().then(function() {

    console.log('Nice! Database looks fine')

}).catch(function(err) {

    console.log(err, "Something went wrong with the Database Update!")

});
// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    console.log(err)
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});
module.exports = app;
